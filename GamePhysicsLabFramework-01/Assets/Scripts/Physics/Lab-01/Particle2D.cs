﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle2D : MonoBehaviour
{
    // Step 1: member variables required for the simple physics functions.
    public Vector2 position, velocity, acceleration;
    public float rotation, angularVel, angularAcc;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Step 3:
        //updatePositionEulerExplicit(Time.deltaTime);
        //UpdatePositionEulerExplicit(Time.fixedUnscaledDeltaTime);
        UpdatePositionKinematic(Time.fixedDeltaTime);
        //UpdateRotationEulerExplicit(Time.fixedDeltaTime);
        UpdateRotationKinematic(Time.fixedDeltaTime);

        // Step 4
        // test by faking motion along a curve.
        // Derivative of cos(t). Which we got as a derivative of sin(t).
        acceleration.x = -Mathf.Sin(Time.time);

        transform.position = position;
        Vector3 v = transform.rotation.eulerAngles;
        v.z = rotation;
        transform.eulerAngles = new Vector3(v.x, v.y, v.z);
    }

    // Step 2: integration algorithm.
    /// <summary>
    /// Simple euler integration algorithm.
    /// </summary>
    /// <param name="dt">time differential</param>
    private void UpdatePositionEulerExplicit(float dt)
    {
        // Explicit formula. We got this from Dan, but it's also in the slides!
        // x(t + dt) = x(t) + v(t)*dt
        // Generic Euler:
        // F(t+dt) = F(t) + f(t)dt
        //                + (dF/dt) dt
        position += velocity * dt;

        // v(t + dt) = v(t) + a(t)dt
        velocity += acceleration * dt;
    }

    /// <summary>
    /// Kinematic formula for position.
    /// </summary>
    /// <param name="dt">Time differential.</param>
    private void UpdatePositionKinematic(float dt)
    {
        // Integrating displacement, second integral of acceleration... Gives the same thing at the end of the day.
        // next time position is current position with the velocity times differential, with the scaled acceleration time differential.
        position = position + (velocity * dt) + (0.5f * acceleration * (dt * dt));

        // Don't forget to update velocity!
        // Same thing as position update in euler's.
        velocity += acceleration * dt;
    }

    /// <summary>
    /// Simple euler integration for rotation. Rotation is a single dimension in 2D!
    /// </summary>
    /// <param name="dt">Time differential.</param>
    private void UpdateRotationEulerExplicit(float dt)
    {
        // Again, explicit formula.
        rotation += angularVel * dt;

        // And again, make sure to update velocity.
        angularVel += angularAcc * dt;
    }

    /// <summary>
    /// Simple kinematic formula for rotation. Rotation is a single dimension in 2D!
    /// </summary>
    /// <param name="dt">Time differential</param>
    private void UpdateRotationKinematic(float dt)
    {
        rotation = rotation + (angularVel * dt) + (0.5f * angularAcc * (dt * dt));

        angularVel += angularAcc * dt;
    }
}
